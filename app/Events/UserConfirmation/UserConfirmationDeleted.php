<?php

namespace App\Events\UserConfirmation;

use App\Models\UserConfirmation;

class UserConfirmationDeleted
{
    /**
     * @var UserConfirmation
     */
    public UserConfirmation $confirmation;

    /**
     * UserConfirmationDeleted constructor.
     * @param UserConfirmation $confirmation
     */
    public function __construct(UserConfirmation $confirmation)
    {
        $this->confirmation = $confirmation;
    }
}
