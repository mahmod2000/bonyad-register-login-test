<?php


namespace App\Events\User;


use App\Models\User;

class UserUpdated
{
    /**
     * @var User
     */
    public $user;

    /**
     * UserUpdated constructor.
     * @param User $user
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }
}
