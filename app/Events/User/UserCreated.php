<?php

namespace App\Events\User;

use App\Models\User;

class UserCreated
{
    /**
     * @var User
     */
    public User $user;

    /**
     * UserCreated constructor.
     * @param User $user
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }
}
