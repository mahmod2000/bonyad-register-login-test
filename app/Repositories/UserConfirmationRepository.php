<?php

namespace App\Repositories;

use App\Events\UserConfirmation\UserConfirmationDeleted;
use App\Models\User;
use App\Models\UserConfirmation;
use Illuminate\Support\Facades\DB;

class UserConfirmationRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'mobile',
        'code'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * @return string
     */
    public function model()
    {
        return UserConfirmation::class;
    }

    /**
     * Find by mobile and code, it finds by Expire Date
     * @param $countryCode
     * @param $mobile
     * @param $code
     * @return mixed
     */
    public function findByMobileAndCode($countryCode, $mobile, $code)
    {
        return $this->model::byMobileAndCode($countryCode, $mobile, $code)->first();
    }

    /**
     * @param array $data
     * @return UserConfirmation|\Illuminate\Database\Eloquent\Model
     * @throws \Exception
     */
    public function createForMobile(array $data)
    {
        /** @var $user */
        $userRepo = $this->app->make(UserRepository::class);

        $item = $this->create([
            'country_code' => $country_code = $data['country_code'],
            'mobile' => $mobile = $data['mobile'],
            'code' => $code = $data['code'],
            'expire_date' => now()->addMinutes(15)->format('Y-m-d H:i:s'),
        ]);

        /** Find user */
        if($findUser = $userRepo->findUserByMobile($country_code, $mobile)) {
            $userRepo->updateUserInfo($findUser, ['password' => bcrypt($code)]);
        }

        return $item;
    }

    /**
     * Confirm implementation and update or make a new user
     *
     * @param UserConfirmation $confirmation
     * @return User
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     * @throws \Exception
     */
    public function confirm(UserConfirmation $confirmation) : User
    {
        DB::beginTransaction();

        try {
            /** @var $user */
            $userRepo = $this->app->make(UserRepository::class);
            $user = $userRepo->findAndUpdateOrMake($confirmation->country_code, $confirmation->mobile, $confirmation->code);

            /** Delete Confirmation item */
            $this->delete($confirmation->id);

            /** Run an event */
            event(new UserConfirmationDeleted($confirmation));
        } catch (\Exception $e) {
            DB::rollBack();

            throw new \Exception($e->getMessage());
        }
        DB::commit();

        return $user;
    }
}
