<?php

namespace App\Repositories;

use App\Enum\UserStatusEnum;
use App\Events\User\UserCreated;
use App\Events\User\UserUpdated;
use App\Models\User;

class UserRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'mobile',
        'first_name',
        'last_name'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * @return string
     */
    public function model()
    {
        return User::class;
    }

    /**
     * Find or make user
     * @param $country_code
     * @param $mobile
     * @param $code
     * @return User
     * @throws \Exception
     */
    public function findAndUpdateOrMake($country_code, $mobile, $code) : User
    {
        /** Update user */
        $user = $this->findUserByMobile($country_code, $mobile);

        if(empty($user)) {
            $user = $this->create([
                'country_code' => $country_code,
                'mobile' => $mobile,
                'password' => bcrypt($code),
                'status' => UserStatusEnum::PENDING
            ]);

            /** Run an event */
            event(new UserCreated($user));

            return $user;
        }

        return $this->updateUserInfo($user, ['status' => UserStatusEnum::COMPLETED]);
    }

    /**
     * @param User $user
     * @param array $data
     * @return User
     * @throws \Exception
     */
    public function updateUserInfo(User $user, array $data) : User
    {
        $user = $this->update($data, $user->id);

        /** Run an event */
        event(new UserUpdated($user));

        return $user;
    }
}
