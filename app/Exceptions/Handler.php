<?php

namespace App\Exceptions;

use App\Http\Controllers\Controller;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Validation\ValidationException;
use Throwable;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'current_password',
        'password',
        'password_confirmation',
    ];

    /**
     * Register the exception handling callbacks for the application.
     *
     * @return void
     */
    public function register()
    {
        $this->reportable(function (Throwable $e) {
            //
        });

        $this->renderable(function (\Exception $e) {
            if($e instanceof ValidationException) {
                return (new Controller())->sendResponseJson(false, ['errors' => $e->errors()], __('validation error'), 422);
            }
            else if($e instanceof AuthenticationException) {
                return (new Controller())->sendResponseJson(false, [], __('auth error'), 401);
            }

            return (new Controller())->sendResponseJson(false, [], $e->getMessage(), $e->getCode());
        });
    }
}
