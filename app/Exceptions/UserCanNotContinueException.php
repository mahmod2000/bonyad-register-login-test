<?php

namespace App\Exceptions;

use App\Http\Controllers\Controller;

class UserCanNotContinueException extends \Exception
{
    /**
     * Report the exception.
     *
     * @return void
     */
    public function report()
    {
    }

    /**
     * Render the exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request
     * @return \Illuminate\Http\JsonResponse
     */
    public function render($request)
    {
        return (new Controller())->sendResponseJson(false, [], $this->message, 401);
    }
}
