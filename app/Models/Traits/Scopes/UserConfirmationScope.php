<?php

namespace App\Models\Traits\Scopes;

use Carbon\Carbon;

trait UserConfirmationScope
{
    /**
     * @param $query
     * @param $countryCode
     * @param $mobile
     * @param $code
     */
    public function scopeByMobileAndCode($query, $countryCode, $mobile, $code)
    {
        $now = now()->format('Y-m-d H:i:s');
        $query
            ->where('country_code', $countryCode)
            ->where('mobile', $mobile)
            ->where('code', $code)
            ->whereRaw("expire_date >= '{$now}'");
    }
}
