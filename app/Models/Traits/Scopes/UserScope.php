<?php

namespace App\Models\Traits\Scopes;

trait UserScope
{
    /**
     * @param $query
     * @param $countryCode
     * @param $mobile
     */
    public function scopeByMobile($query, $countryCode, $mobile)
    {
        $query
            ->where('country_code', $countryCode)
            ->where('mobile', $mobile);
    }
}
