<?php

namespace App\Models;

use App\Models\Traits\Scopes\UserConfirmationScope;
use App\Models\Traits\Relations\UserConfirmationRelation;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UserConfirmation extends Model
{
    use HasFactory, SoftDeletes, UserConfirmationRelation, UserConfirmationScope;

    protected $fillable = [
        'country_code',
        'mobile',
        'code',
        'expire_date'
    ];
}
