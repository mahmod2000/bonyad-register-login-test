<?php

namespace App\Enum;


class UserStatusEnum extends EnumStateManager
{
    const PENDING = 1;
    const COMPLETED = 2;
    const BLOCKED = 3;

    static protected $names = [
        self::PENDING => 'در انتظار تکمیل اطلاعات',
        self::COMPLETED => 'تکمیل شده',
        self::BLOCKED => 'مسدود شده',
    ];
}
