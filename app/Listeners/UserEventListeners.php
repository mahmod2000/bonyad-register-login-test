<?php

namespace App\Listeners;

use App\Events\User\UserCreated;
use App\Events\User\UserUpdated;

class UserEventListeners
{
    /**
     * @param $event
     */
    public function onCreated($event)
    {
        // TODO $event->user is available;
    }

    /**
     * @param $event
     */
    public function onUpdated($event)
    {
        // TODO $event->user is available;
    }

    /**
     * @param $events
     */
    public function subscribe($events)
    {
        $events->listen(
            UserCreated::class,
            'App\Listeners\UserEventListeners@onCreated'
        );

        $events->listen(
            UserUpdated::class,
            'App\Listeners\UserEventListeners@onUpdated'
        );
    }
}
