<?php

namespace App\Listeners;

use App\Events\User\UserCreated;
use App\Events\User\UserUpdated;
use App\Events\UserConfirmation\UserConfirmationDeleted;

class UserConfirmationEventListeners
{
    /**
     * @param $event
     */
    public function onDeleted($event)
    {
        // TODO $event->confirmation is available;
    }

    /**
     * @param $events
     */
    public function subscribe($events)
    {
        $events->listen(
            UserConfirmationDeleted::class,
            'App\Listeners\UserConfirmationEventListeners@onDeleted'
        );
    }
}
