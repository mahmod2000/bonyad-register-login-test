<?php

namespace App\Http\Controllers\Api;

use App\Enum\UserStatusEnum;
use App\Http\Controllers\Controller;
use App\Http\Requests\User\UserUpdateInformationRequest;
use App\Http\Resources\UserResource;
use App\Repositories\UserRepository;

class UserController extends Controller
{
    /**
     * @var UserRepository
     */
    private UserRepository $userRepository;

    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    /**
     * Get the authenticated User.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function me(): \Illuminate\Http\JsonResponse
    {
        return $this->sendResponseJson(true, new UserResource(auth()->user()), __('user information'));
    }

    /**
     * @param UserUpdateInformationRequest $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function update(UserUpdateInformationRequest $request): \Illuminate\Http\JsonResponse
    {
        /** @var $user */
        $user = $this->userRepository->updateUserInfo(auth()->user(), $request->merge(['status' => UserStatusEnum::COMPLETED])->all());

        return $this->sendResponseJson(true, new UserResource($user), __('user updated'));
    }

    /**
     * Log the user out (Invalidate the token).
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout(): \Illuminate\Http\JsonResponse
    {
        auth()->logout();

        return $this->sendResponseJson(true, [], __('user loggedOut'));
    }

    /**
     * Refresh a token.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function refresh(): \Illuminate\Http\JsonResponse
    {
        return $this->sendResponseJson(true, $this->makeResponseToken(auth()->refresh()));
    }
}
