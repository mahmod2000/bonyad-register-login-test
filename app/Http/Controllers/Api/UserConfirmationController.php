<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\UserConfirmation\UserConfirmationStoreRequest;
use App\Http\Requests\UserConfirmation\UserConfirmationVerifyRequest;
use App\Http\Resources\UserConfirmationResource;
use App\Http\Resources\UserResource;
use App\Jobs\SendSMSJob;
use App\Models\UserConfirmation;
use App\Repositories\UserConfirmationRepository;
use App\Repositories\UserRepository;

class UserConfirmationController extends Controller
{
    /**
     * @var
     */
    private $token;

    /**
     * @var
     */
    private $confirmation;

    /**
     * @var UserConfirmationRepository
     */
    private UserConfirmationRepository $confirmationRepository;

    /**
     * @var UserRepository
     */
    private UserRepository $userRepository;

    public function __construct(UserConfirmationRepository $confirmationRepository, UserRepository $userRepository)
    {
        $this->confirmationRepository = $confirmationRepository;
        $this->userRepository = $userRepository;
    }

    /**
     * Store code for user confirmation
     * @param UserConfirmationStoreRequest $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function send(UserConfirmationStoreRequest $request): \Illuminate\Http\JsonResponse
    {
        /** Add a code for sending to user */
        $request->merge(['code' => $code = rand(444444,999999)]);

        /** @var $item */
        $item = $this->confirmationRepository->createForMobile($request->all());

        /** Send SMS */
        SendSMSJob::dispatch($request->get('country_code'), $request->get('mobile'), $request->get('code'));

        return $this->sendResponseJson(true, new UserConfirmationResource($item), __('made message'));
    }

    /**
     * @param UserConfirmationVerifyRequest $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     * @throws \Exception
     */
    public function verify(UserConfirmationVerifyRequest $request): \Illuminate\Http\JsonResponse
    {
        /** @var $confirmation */
        $this->confirmation = $this->confirmationRepository->findByMobileAndCode($request->get('country_code'), $request->get('mobile'), $request->get('code'));
        if(empty($this->confirmation)) {
            return $this->sendResponseJson(false, [], __('not found confirmation code'), 404);
        }

        /** @var $user */
        $user = $this->findAndLoginUser($request->get('code'));

        /** Make data for response */
        $data = [
            'user' => new UserResource($user),
            'token' => $this->makeResponseToken($this->token)
        ];

        if($user->wasRecentlyCreated) { /** We can find the user is added newly */
            $data['code'] = 1111;
            $message = __('user logged in and leads to info page');
        }
        else {
            $data['code'] = 2222;
            $message = __('user logged in');
        }

        return $this->sendResponseJson(true, $data, $message);
    }

    /**
     * @param $code
     * @return \App\Models\User
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     * @throws \Exception
     */
    public function findAndLoginUser($code): \App\Models\User
    {
        /** @var $data */
        $user = $this->confirmationRepository->confirm($this->confirmation);

        /** @var $token */
        $this->token = auth('api')->attempt(['mobile' => $user->mobile, 'password' => $code]);

        /** Update user password for security */
        $this->userRepository->updateUserInfo($user, ['password' => bcrypt(rand(4444444,8999999))]);

        return $user;
    }
}
