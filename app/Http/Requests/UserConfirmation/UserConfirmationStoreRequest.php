<?php

namespace App\Http\Requests\UserConfirmation;

use App\Rules\MobileRule;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class UserConfirmationStoreRequest extends FormRequest
{
    public function rules()
    {
        return [
            'country_code' => ['required', Rule::in(['+98'])],
            'mobile' => ['required', new MobileRule]
        ];
    }
}
