<?php

namespace App\Http\Requests\UserConfirmation;

use App\Rules\MobileRule;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class UserConfirmationVerifyRequest extends FormRequest
{
    public function rules()
    {
        return [
            'country_code' => ['required', Rule::in(['+98'])],
            'mobile' => ['required', new MobileRule, 'exists:user_confirmations,mobile'],
            'code' => ['required', 'numeric'],
        ];
    }

    public function messages()
    {
        return [
            'mobile.exists' => __('validation exists mobile message')
        ];
    }
}
