<?php

namespace App\Http\Resources;

use App\Enum\UserStatusEnum;
use Illuminate\Http\Resources\Json\JsonResource;

class UserResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'first_name' => $this->first_name,
            'last_name' => $this->last_name,
            'country_code' => $this->country_code,
            'mobile' => $this->mobile,
            'status' => $this->status,
            'status_text' => UserStatusEnum::getName($this->status),
            'created_at' => $this->created_at->format('Y-m-d H:i:s'),
        ];
    }
}
