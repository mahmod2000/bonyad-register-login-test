<?php

namespace App\Http\Middleware;

use App\Enum\UserStatusEnum;
use App\Exceptions\UserCanNotContinueException;
use Closure;
use Illuminate\Http\Request;

class CheckUserCompletedDataMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     * @throws \Throwable
     */
    public function handle(Request $request, Closure $next)
    {
        throw_if((auth()->check() and auth()->user()->status == UserStatusEnum::PENDING), new UserCanNotContinueException(__('user not completed data and can not process'), 401));

        return $next($request);
    }
}
