<?php

namespace Tests;

use Illuminate\Foundation\Testing\TestCase as BaseTestCase;
use Illuminate\Support\Facades\Log;
use Tymon\JWTAuth\Facades\JWTAuth;

abstract class TestCase extends BaseTestCase
{
    use CreatesApplication;

    const MOBILES = ['09194747602', '09124545222', '09127456545'];


    /**
     * Set the currently logged in user for the application.
     *
     * @param  \Illuminate\Contracts\Auth\Authenticatable $user
     * @param  string|null                                $driver
     * @return $this
     */
    public function actingAs($user, $driver = null)
    {
        $token = JWTAuth::fromUser($user);
        $this->withHeader('Authorization', "Bearer {$token}");
        parent::actingAs($user);

        return $this;
    }

    /**
     * @param string $model
     * @param array $attributes
     * @param bool $resource
     * @return mixed
     */
    public function create(string $model, array $attributes = [], $resource = true)
    {
        $make_model = "App\\Models\\$model";
        $resourceModel = $make_model::factory()->create($attributes);

        if(!$resource) return $resourceModel;

        $resourceClass = "App\\Http\\Resources\\{$model}Resource";
        return new $resourceClass($resourceModel);
    }

    /**
     * @param $log_name
     * @param $data
     */
    public function addLog($log_name, $data)
    {
        Log::info($log_name, [$data]);
    }
}
