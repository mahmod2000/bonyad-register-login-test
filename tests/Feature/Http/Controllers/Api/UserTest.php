<?php

namespace Tests\Feature\Http\Controllers\Api;

use App\Enum\UserStatusEnum;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class UserTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    /**
     * @test
     */
    public function can_not_see_information_on_non_logged_user()
    {
        $response = $this->json('GET', 'api/me');
        $response
            ->assertStatus(401)
            ->assertJson([
                'success' => false,
            ]);
    }

        /**
     * @test
     */
    public function can_see_information_on_pending_status()
    {
        $user = $this->create("User", ['mobile' => $mobile = self::MOBILES[0], 'status' => UserStatusEnum::PENDING], false);

        $response = $this->actingAs($user)->json('GET','api/me');

        $response
            ->assertOk()
            ->assertJson([
                'success' => true,
                'data' => [
                    'mobile' => $mobile,
                    'status' => UserStatusEnum::PENDING
                ]
            ]);
    }

    /**
     * @test
     */
    public function can_not_logout_because_of_not_completed_information()
    {
        $user = $this->create("User", ['mobile' => $mobile = self::MOBILES[0], 'status' => UserStatusEnum::PENDING], false);

        $response = $this->actingAs($user)->json('GET','api/logout');

        $response
            ->assertStatus(401)
            ->assertJson([
                'success' => false
            ]);
    }

    /**
     * @test
     */
    public function can_update_information_and_complete()
    {
        $user = $this->create("User", ['mobile' => $mobile = self::MOBILES[0], 'status' => UserStatusEnum::PENDING], false);

        $response = $this->actingAs($user)->json('POST','api/update', [
            'first_name' => $firstName = $this->faker->firstName,
            'last_name' => $this->faker->lastName,
        ]);

        $response
            ->assertOk()
            ->assertJson([
                'success' => true,
                'data' => [
                    'first_name' => $firstName,
                    'mobile' => $mobile,
                    'status' => UserStatusEnum::COMPLETED
                ]
            ]);
    }
}
