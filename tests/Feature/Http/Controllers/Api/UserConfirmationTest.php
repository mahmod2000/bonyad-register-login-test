<?php

namespace Tests\Feature\Http\Controllers\Api;

use App\Models\UserConfirmation;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class UserConfirmationTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    /**
     * @test
     */
    public function can_not_make_send_confirmation_code_because_of_validation_error()
    {
        $response = $this->json('POST','api/send', [
            'country_code' => '+9',
            'mobile' => $this->faker->randomElement(self::MOBILES)
        ]);

        $response
            ->assertStatus(422)
            ->assertJson([
                'success' => false
            ]);
    }

    /**
     * @test
     */
    public function can_make_and_send_confirmation_code()
    {
        $response = $this->json('POST','api/send', [
            'country_code' => '+98',
            'mobile' => $this->faker->randomElement(self::MOBILES)
        ]);

        $response
            ->assertOk()
            ->assertJsonStructure([
                'success',
                'data' => [
                    'code'
                ],
                'message'
            ]);
    }

    /**
     * @test
     */
    public function can_verify_confirmation_code()
    {
        $confirmation = $this->create("UserConfirmation");

        $response = $this->json('POST','api/verify', [
            'country_code' => $confirmation->country_code,
            'mobile' => $confirmation->mobile,
            'code' => $confirmation->code
        ]);

        $response
            ->assertOk()
            ->assertJsonStructure([
                'success',
                'data' => [
                    'user' => [
                        'id'
                    ],
                    'token' => [
                        'access_token'
                    ]
                ],
                'message'
            ]);
    }
}
