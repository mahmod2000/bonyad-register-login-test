<?php

namespace Database\Factories;

use App\Models\UserConfirmation;
use Illuminate\Database\Eloquent\Factories\Factory;

class UserConfirmationFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = UserConfirmation::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'country_code' => '+98',
            'mobile' => $this->faker->randomElement(['09194747602', '09124545222', '09127456545']),
            'code' => rand(444444, 999999),
            'expire_date' => now()->addMinutes(15)->format('Y-m-d H:i:s')
        ];
    }
}
