<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserConfirmationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_confirmations', function (Blueprint $table) {
            $table->id();
            $table->string('country_code', 5);
            $table->string('mobile', 15)->index();
            $table->string('code', 8);
            $table->dateTime('expire_date')->nullable();
            $table->timestamps();
            $table->softDeletes();

            /** Speedup the query on searching these 3 columns */
            $table->index(['mobile', 'code', 'expire_date']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_confirmations');
    }
}
