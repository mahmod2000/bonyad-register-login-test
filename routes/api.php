<?php

Route::group(['namespace' => 'Api'], function (){

    /*********************** User ****************/
    Route::group(['middleware' => 'auth:api'], function (){
        Route::get('/me', [\App\Http\Controllers\Api\UserController::class, 'me']);
        Route::post('/update', [\App\Http\Controllers\Api\UserController::class, 'update']);
        Route::get('/logout', [\App\Http\Controllers\Api\UserController::class, 'logout'])->middleware('checkUserCompletedDataMiddleware');
        Route::get('/refresh', [\App\Http\Controllers\Api\UserController::class, 'refresh']);
    });

    /*********************** User confirmation *******************/

    /** This route accepts 1 request per 2 minutes, so if the user wants to resend code, should waits passed 2 minutes */
    Route::post('/send', [\App\Http\Controllers\Api\UserConfirmationController::class, 'send']);
        //->middleware('throttle:1,2');

    Route::post('/verify', [\App\Http\Controllers\Api\UserConfirmationController::class, 'verify']);
});
